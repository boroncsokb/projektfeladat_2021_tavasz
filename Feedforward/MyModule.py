import torch
from torch import nn
import random


class FeedForward_type1(nn.Module):
    def __init__(self, input_size, mean, std):
        super(FeedForward_type1, self).__init__()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(input_size, input_size*2),
            nn.ReLU(),
            nn.Linear(input_size*2, input_size),
            nn.ReLU(),
            nn.Linear(input_size, 2))
        self.mean=mean
        self.std=std

    def forward(self, x):
        pred = self.linear_relu_stack(x)
        return pred
    

class FeedForward_type2(nn.Module):
    def __init__(self, input_size, mean, std):
        super(FeedForward_type2, self).__init__()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(input_size, input_size*2),
            nn.ReLU(),
            nn.Linear(input_size*2, input_size*2),
            nn.ReLU(),
            nn.Linear(input_size*2, input_size),
            nn.ReLU(),
            nn.Linear(input_size, 2))
        self.mean=mean
        self.std=std

    def forward(self, x):
        pred = self.linear_relu_stack(x)
        return pred

    
class DatasetFast():
    def __init__(self, data, batch_size, device):
        self.__X = torch.tensor(data.drop(columns=['X_pos2', 'Y_pos2']).to_numpy(dtype='float32'), device=device)
        self.__Y = torch.tensor(data[['X_pos2', 'Y_pos2']].to_numpy(dtype='float32'), device=device)
        self.__batch_size=batch_size
        
        batch_num=int(self.__Y.shape[0]/self.__batch_size)
        if self.__Y.shape[0]%self.__batch_size!=0:
            batch_num+=1        
        self.__batch_num=batch_num
        
        self.__create_batches()
        
    
    def __create_batches(self):
        self.__batches_X=[]
        self.__batches_Y=[]
        
        for i in range(self.__batch_num):
        
            if i!=self.__batch_num-1:
                self.__batches_X.append(self.__X[torch.arange(i*self.__batch_size,(i+1)*self.__batch_size)])
                self.__batches_Y.append(self.__Y[torch.arange(i*self.__batch_size,(i+1)*self.__batch_size)])
            else:
                self.__batches_X.append(self.__X[torch.arange(i*self.__batch_size,self.__Y.shape[0])])
                self.__batches_Y.append(self.__Y[torch.arange(i*self.__batch_size,self.__Y.shape[0])])
        
    
    def shuffle(self):
        index = [i for i in range(len(self.__Y))]
        random.shuffle(index)
        self.__X=self.__X[index]
        self.__Y=self.__Y[index]
        self.__create_batches()
                
    def batch(self,index):
        return self.__batches_X[index], self.__batches_Y[index]
    
    @property
    def input_size(self):
        X, _ = self.batch(0)
        return X.shape[1]
    @property
    def output_size(self):
        _, Y = self.batch(0)
        return Y.shape[1]
    
    @property    
    def batch_size(self):
        return self.__batch_size
    
    @property    
    def batch_num(self):
        return self.__batch_num