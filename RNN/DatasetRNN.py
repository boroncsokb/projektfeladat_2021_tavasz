import torch
import random

class DatasetFastRNN():
    def __init__(self, init_tensor, input_tensor, output_tensor, batch_size, device):
        self.__batch_size=batch_size
        
        self.__init_tensor=init_tensor.to(device)
        self.__input_tensor=input_tensor.to(device)
        self.__output_tensor=output_tensor.to(device)
        
        batch_num=int(self.__init_tensor.shape[0]/self.__batch_size)
        if self.__init_tensor.shape[0]%self.__batch_size!=0:
            batch_num+=1        
        self.__batch_num=batch_num
        
        self.__create_batches()
            
    
    def __create_batches(self):
        self.__batches_init=[]
        self.__batches_input=[]
        self.__batches_output=[]
        
        for i in range(self.__batch_num):
        
            if i!=self.__batch_num-1:
                self.__batches_init.append(self.__init_tensor[torch.arange(i*self.__batch_size,(i+1)*self.__batch_size)])
                self.__batches_input.append(self.__input_tensor[torch.arange(i*self.__batch_size,(i+1)*self.__batch_size)])
                self.__batches_output.append(self.__output_tensor[torch.arange(i*self.__batch_size,(i+1)*self.__batch_size)])
            else:
                self.__batches_init.append(self.__init_tensor[torch.arange(i*self.__batch_size,self.__init_tensor.shape[0])])
                self.__batches_input.append(self.__input_tensor[torch.arange(i*self.__batch_size,self.__init_tensor.shape[0])])
                self.__batches_output.append(self.__output_tensor[torch.arange(i*self.__batch_size,self.__init_tensor.shape[0])])
        
    
    def shuffle(self):
        index = [i for i in range(len(self.__init_tensor))]
        random.shuffle(index)
        self.__init_tensor=self.__init_tensor[index]
        self.__input_tensor=self.__input_tensor[index]
        self.__output_tensor=self.__output_tensor[index]
        self.__create_batches()
                
    def batch(self,index):
        return self.__batches_init[index], self.__batches_input[index], self.__batches_output[index]
    
    @property
    def init_size(self):
        I, _, _ = self.batch(0) #I.shape==torch.Size([batch_size, init_size])
        return I.shape[1]
    
    @property
    def input_size(self):
        _, X, _ = self.batch(0) #X.shape==torch.Size([batch_size, series_len, input_size])
        return X.shape[2]
    
    @property
    def output_size(self):
        _, _, Y = self.batch(0) #Y.shape==torch.Size([batch_size, series_len, output_size])
        return Y.shape[2]
    
    @property
    def series_len(self):
        _, _, Y = self.batch(0)
        return Y.shape[1]
    
    @property    
    def batch_size(self):
        return self.__batch_size
    
    @property    
    def batch_num(self):
        return self.__batch_num