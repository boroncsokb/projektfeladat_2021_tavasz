import torch
from torch import nn

class RNN(nn.Module):
    def __init__(self, init_size, input_size, output_size, hidden_size):
        super(RNN, self).__init__()
        self.init_size=init_size
        self.input_size=input_size
        self.hidden_size=hidden_size
        
        self.initial=nn.Linear(init_size,hidden_size)
        self.hidden=nn.Linear(input_size + hidden_size, input_size + hidden_size)
        self.h2ho = nn.Linear(input_size + hidden_size, hidden_size)
        self.h2o = nn.Linear(input_size + hidden_size, output_size)
        
        self.ReLU=nn.ReLU()

    def forward(self, input, hidden_input, init=False):
        if init:
            hidden_input=self.initial(hidden_input)
            hiddden_input=self.ReLU(hidden_input)
            
        combined = torch.cat((input, hidden_input), -1)
        hidden=self.hidden(combined)
        hidden=self.ReLU(hidden)
        
        hidden_output = self.h2ho(hidden)
        hidden_output=self.ReLU(hidden_output)
        output = self.h2o(hidden)
        
        return output, hidden_output