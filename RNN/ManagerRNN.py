import numpy as np
import torch
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt

#Custom classes
from ModelRNN import RNN
from DatasetRNN import DatasetFastRNN


class ManagerRNN:
    def __init__(self,
                 model: RNN,
                 norm_params: str,
                 device: torch.device,
                 criterion: torch.nn.Module,
                 optimizer: torch.optim.Optimizer,
                 dataset_train: DatasetFastRNN,
                 dataset_test: DatasetFastRNN = None,
                 epochs: int = 100,
                 periodic_save=False,
                 final_save=False,
                 period=None,
                 model_dir=None,
                 notebook: bool = False):

        self.model = model
        self.norm_params = pd.read_csv(norm_params)
        self.criterion = criterion
        self.optimizer = optimizer
        self.dataset_train = dataset_train
        self.dataset_test = dataset_test
        self.device = device
        self.epochs = epochs
        self.periodic_save = periodic_save
        self.final_save = final_save
        self.period = period
        self.model_dir = model_dir
        self.notebook = notebook

        self.training_loss = []
        self.test_loss = []

    def run_trainer(self):

        if self.notebook:
            from tqdm.notebook import tqdm, trange
        else:
            from tqdm import tqdm, trange

        progressbar = trange(self.epochs, desc='Progress')
        for i in progressbar:
            """Training block"""
            self._train(progressbar)
            """Validation block"""
            if self.dataset_test is not None:
                self._test(progressbar)
            """Model save"""
            if self.periodic_save and (i + 1) % self.period == 0 and (
                    i + 1) != self.epochs:
                self.save_model(i + 1, self.test_loss[-1])

        if self.final_save:
            self.save_model(self.epochs, self.test_loss[-1])

        return self.training_loss, self.test_loss

    def _train(self, pr_bar):

        if self.notebook:
            from tqdm.notebook import tqdm, trange
        else:
            from tqdm import tqdm, trange

        self.model.train()  # train mode
        train_losses = []  # accumulate the losses here

        batch_iter = tqdm(range(self.dataset_train.batch_num),
                          'Training',
                          leave=False)

        for i in batch_iter:
            I, X, Y = self.dataset_train.batch(i)
            train_losses.append(self._train_batch(I, X, Y))

        avg_loss = np.mean(train_losses)

        pr_bar.set_description(f'Progress (avg training loss: {avg_loss:.5f})'
                               )  # update progressbar

        self.training_loss.append(avg_loss)

        batch_iter.close()

    def _test(self, pr_bar):

        if self.notebook:
            from tqdm.notebook import tqdm, trange
        else:
            from tqdm import tqdm, trange

        self.model.eval()  # evaluation mode
        test_losses = []  # accumulate the losses here

        batch_iter = tqdm(range(self.dataset_test.batch_num),
                          'Test',
                          leave=False)

        for i in batch_iter:
            I, X, Y = self.dataset_test.batch(i)
            test_losses.append(self._test_batch(I, X, Y))

        avg_loss = np.mean(test_losses)
        pr_bar.set_description(
            f'Progress (avg test loss: {avg_loss:.5f})')  # update progressbar

        self.test_loss.append(avg_loss)

        batch_iter.close()

    def _train_batch(self, X_init, X, Y):
        output = torch.zeros(Y.shape).to(self.device)
        hidden = torch.zeros(Y.shape[0],
                             self.model.hidden_size).to(self.device)

        for i in range(X.shape[1]):
            if i == 0:
                output[:, i, :], hidden = self.model(X[:, i, :],
                                                     X_init,
                                                     init=True)
            else:
                output[:, i, :], hidden = self.model(X[:, i, :], hidden)

        # Backpropagation
        self.optimizer.zero_grad()
        loss = self.criterion(output, Y)
        loss.backward()
        self.optimizer.step()

        return loss.item()

    def _test_batch(self, X_init, X, Y):
        with torch.no_grad():
            output = torch.zeros(Y.shape).to(self.device)
            hidden = torch.zeros(Y.shape[0],
                                 self.model.hidden_size).to(self.device)

            for i in range(X.shape[1]):
                if i == 0:
                    output[:, i, :], hidden = self.model(X[:, i, :],
                                                         X_init,
                                                         init=True)
                else:
                    output[:, i, :], hidden = self.model(X[:, i, :], hidden)

            loss = self.criterion(output, Y)

        return loss.item()

    def predictSeries(self, data):
        
        if self.notebook:
            from tqdm.notebook import tqdm, trange
        else:
            from tqdm import tqdm, trange        
        
        init1 = torch.tensor(data.iloc[0].drop('dt').to_numpy(dtype='float32'),
                             device=self.device)
        init2 = torch.tensor(data.iloc[1].to_numpy(dtype='float32'),
                             device=self.device)
        init_tensor = torch.cat((init1, init2), 0)

        data = data.drop([0, 1])
        data.reset_index(drop=True, inplace=True)
        input_tensor = torch.tensor(
            data.drop(columns=['X_pos', 'Y_pos', "Z_pos"]).to_numpy(dtype='float32'),
            device=self.device)
        output = torch.zeros(input_tensor.shape[0], 3).to(self.device)

        with torch.no_grad():
            hidden = torch.zeros(1, self.model.hidden_size).to(self.device)

            bar = tqdm(range(input_tensor.shape[0]), 'Predicting', leave=False)

            for i in bar:
                if i == 0:
                    output[i, :], hidden = self.model(input_tensor[i, :],
                                                      init_tensor,
                                                      init=True)
                else:
                    output[i, :], hidden = self.model(input_tensor[i, :],
                                                      hidden)

        params = self.norm_params[['X_pos', 'Y_pos', 'Z_pos']]
        mean = torch.tensor(params.iloc[0].to_numpy(dtype='float32'),
                            device=self.device).repeat(output.shape[0], 1)
        std = torch.tensor(params.iloc[1].to_numpy(dtype='float32'),
                           device=self.device).repeat(output.shape[0], 1)

        return output * std + mean

    def plotLosses(self):
        e=np.arange(self.epochs)+1
        plt.figure(figsize=(14,6))
        plt.plot(e,self.training_loss,label='Train loss')
        plt.plot(e,self.test_loss,label='Test loss')
        plt.title("Average loss through the epochs",pad= 20)
        plt.xlabel("Epoch")
        plt.ylabel("Average loss")
        plt.legend(loc='upper right')
        plt.yscale("log")
        plt.tight_layout()
        plt.show()
        
    def testAccuracy(self, data):
        pred=self.predictSeries(data).to('cpu')
        pred = pd.DataFrame(pred, columns = ['X_pos','Y_pos', 'Z_pos'])

        params=self.norm_params[['X_pos', 'Y_pos', 'Z_pos']]

        ref=data.drop([0, 1])
        ref.reset_index(drop=True, inplace=True)
        ref=ref[['X_pos','Y_pos', 'Z_pos']]
        #Renormalizing reference
        ref=ref*params.iloc[1]+params.iloc[0]

        plt.figure(figsize=(14,10))
        plt.subplot(211)
        plt.plot(pred['X_pos'],label='Predicted X movement')
        plt.plot(ref['X_pos'],label='Real X movement')
        plt.plot(pred['Y_pos'],label='Predicted Y movement')
        plt.plot(ref['Y_pos'],label='Real Y movement')
        plt.plot(pred['Z_pos'],label='Predicted Z movement')
        plt.plot(ref['Z_pos'],label='Real Z movement')
        plt.title("Predicted vs Real movement",pad= 20)
        plt.xlabel("Timestep")
        plt.ylabel("X/Y/Z Position [mm]")
        plt.legend(loc='upper right')
        plt.tight_layout()

        plt.subplot(212)
        plt.plot(pred['X_pos']-ref['X_pos'],label='X_error')
        plt.plot(pred['Y_pos']-ref['Y_pos'],label='Y_error')
        plt.plot(pred['Z_pos']-ref['Z_pos'],label='Z_error')
        plt.title("Prediction error",pad= 20)
        plt.xlabel("Timestep")
        plt.ylabel("Error [mm]")
        plt.legend(loc='upper right')
        plt.tight_layout()
        plt.show()

    def save_model(self, epoch, loss):
        output_name = "model_" + now() + '_epoch_' + str(
            epoch) + f'_loss_ {loss:.4f}_' + '.pth'
        torch.save(self.model, self.model_dir + output_name)


def now():
    return datetime.now().strftime("%Y-%m-%d_%H-%M-%S")